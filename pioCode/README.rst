Setup
=====

#. Clone git repo `git clone git@bitbucket.org:benjaminbatya/ringtheory.git` 
#. copy platform.ini.config -> platform.ini
#. Set the `upload_port` and `monitor_port` depending on your host (build machine)
#. install platformio
#. install vscode
#. install platformio extension in vscode
#. Open vscode in this folder so the .vscode folder is automatically generated.
#. (in linux) Copy ../udev/99-pololu.rules to /etc/udev/rules.d/
#. try building the project by using the PlatformIO project tasks
