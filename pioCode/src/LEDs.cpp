
#include "LEDs.hpp"

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    3

namespace LEDs {

::uint8_t RED = 0;     // 0deg
::uint8_t YELLOW = 43; // Gold -> 60deg
::uint8_t GREEN = 85; // Green -> 120deg
::uint8_t CYAN = 128; // Cyan -> 180deg
::uint8_t BLUE = 170; // Blue -> 240deg
::uint8_t PURPLE = 213; // Purple -> 300deg
::uint8_t WHITE = 255; // Special case, equals saturation==0

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LEDS_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

void init() {
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.clear();
  strip.setBrightness(255); // Set BRIGHTNESS to about 1/5 (max = 255)
}

uint8_t _value = 0xff;

void incValue() {
  if(_value < 0xff) { _value+=0x05; }
}

void decValue() {
  if(_value > 0x00) { _value-=0x05; }
}

uint8_t getValue() { return _value; }

static uint8_t colorIdx = 0;
static const uint8_t MAX_COLORS = 255;

void incColorIdx() {
  colorIdx = (colorIdx+1) % MAX_COLORS;
}

void setColorIdx(uint8_t newIdx) {
  colorIdx = newIdx % MAX_COLORS;
}

uint8_t getColorIdx() { return colorIdx; }

}; // namespace LEDs