#include <stdint.h>

#include "LEDs.hpp"
#include "Buttons.hpp"
#include "Accel.hpp"
#include "main.hpp"

namespace PaintMode {

// Represents a pixel. 
// If hue is 0, then its off. 1-255 are valid color ranges
struct Pixel {
  uint8_t hue;

};

// Use a sectional approach to make editing easier
Pixel pixels[LEDS_IN_SECTION] = {0x0};
uint8_t PixelOffset = 0;

// Various edit states
bool editState = false;
uint8_t editHue = 0x40;

#define NUM_SAMPLES 4
uint8_t editPosSamples[NUM_SAMPLES] = {0};
uint8_t editSampleIdx = 0;
uint8_t editPosAvg = 0;
unsigned long cursorDisplayLastTime;
bool cursorDisplay = false;

int editShakeNumSamples = 0;
unsigned long editShakeLastTime;

void Setup() {
  memset(pixels, 0, sizeof(pixels));

  PixelOffset = 0;
  editState = true;
  editHue = 0x40;
  editShakeLastTime = millis();
  cursorDisplayLastTime = editShakeLastTime;
  cursorDisplay = false;
}

void HandleButtons();
void HandleShaking();
void Animate();
void Render();

void Update() {
  // HandleButtons();
  HandleShaking();

  Animate();

  Render();
}

void HandleButtons() {
  // Check the buttons
  const auto& buttons = Buttons::check();

  if(buttons.checked) {
    CheckIncMode(buttons);

    if(buttons.state[Buttons::BtnA2] == Buttons::StatePressed) {
      // Serial.print("toggling edit state"); Serial.println(editState);
      editState = !editState;
    }
    if(editState) {
      if(buttons.state[Buttons::BtnB1] == Buttons::StateDown) {
        // Serial.println("+");
        editHue += 3;
      }
      if(buttons.state[Buttons::BtnB2] == Buttons::StateDown) {
        // Serial.println("-");
        editHue -= 3;
      }
    }

    CheckSpeedControls(buttons);
  }
}

void HandleShaking() {

  // check shake state
  if(!editState) { return; }
    
  uint16_t accel2 = Accel::accelMag2();

  // if accel is high enough, generate a particle
  if(accel2 <= 5000) { return; }
  // Serial.print("accel2="); Serial.println(accel2);

  uint16_t xzAccel2 = Accel::xzMag2();

  if(xzAccel2 <= 800) { return; }
  // Serial.print("xzAccel2="); Serial.println(xzAccel2);

  auto yAccel = Accel::yValue(); 
  // treat shaking as important only if there's a large enough Y component
  if(abs(yAccel) < 10000) { return; }
  // Serial.print("yAccel="); Serial.println(yAccel);
      
  auto now = millis();
  if(now - editShakeLastTime >= 250) {
    // Serial.print("editShakeNumSamples="); Serial.println(editShakeNumSamples);

    // find the right pixel
    auto pixelIdx = (editPosAvg - PixelOffset) % LEDS_IN_SECTION;

    // set the color
    auto newHue = editHue;
    if(pixels[pixelIdx].hue != 0) newHue = 0;

    pixels[pixelIdx].hue = newHue;
    // Serial.printf("Setting pixel %d to %x, editPosSamples=%d\n", pixelIdx, newHue, editPosSamples);
    editShakeLastTime = now;
  }
}

void Animate() {
  if(editState) { return; }

  if(PixelOffset==0) { PixelOffset = LEDS_IN_SECTION-1; }
  else { PixelOffset -= 1; }
  // Serial.print("Animate: PixelOffset="); Serial.println(PixelOffset);
}

void Render() {

  for(uint8_t ii=0; ii<LEDS_IN_SECTION; ++ii) {
    uint8_t hue = pixels[ii].hue;
    uint8_t value = 0xff;
    if(hue==0) { value = 0x0; }

    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(hue)<<8, 0xff, value);
    color = Adafruit_NeoPixel::gamma32(color);

    auto idx = (ii+PixelOffset)%LEDS_IN_SECTION;

    for(uint8_t jj=0; jj<LEDS_NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(idx+jj*LEDS_IN_SECTION, color);
    }
  }

  if(editState) {
    // highlight the currently selected pixel
    float fIdx = Accel::interpolatedTilt();
    auto idx = uint8_t(fIdx*LEDS_COUNT); 
    idx = min(LEDS_COUNT-1, idx);
    idx %= LEDS_IN_SECTION;

    // average the position samples because the pixel bounces around too much
    editPosSamples[editSampleIdx] = idx;
    editSampleIdx = (editSampleIdx+1)%NUM_SAMPLES;
    uint16_t avg = 0;
    for(int ii=0; ii<NUM_SAMPLES; ++ii) { avg += editPosSamples[ii]; }
    editPosAvg = avg/NUM_SAMPLES;

    auto now = millis();
    bool switchDisplay = cursorDisplay ? (now-cursorDisplayLastTime > 350) : (now-cursorDisplayLastTime > 150);
    if(switchDisplay) {
      cursorDisplay = !cursorDisplay;
      cursorDisplayLastTime = now;
    }

    uint8_t sat = cursorDisplay ? 0xff : 0x0;
    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(editHue)<<8, sat, 0xff);
    color = Adafruit_NeoPixel::gamma32(color);

    for(uint8_t jj=0; jj<LEDS_NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(editPosAvg+jj*LEDS_IN_SECTION, color);
    }
  }
}

}; // namespace PaintMode