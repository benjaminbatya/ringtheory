#include "Accel.hpp"
#include "LEDs.hpp"
#include "main.hpp"
#include "Buttons.hpp"

namespace OppositeMode {

uint32_t color = 0;

#define MIN_PIXELS 2
#define MAX_PIXELS 11
uint8_t numPixels;

void Setup() {
  numPixels = MIN_PIXELS;
}

void Update() {
  {
    LEDs::strip.clear();
  }

  float fIdx = Accel::interpolatedTilt();

  for(int ii=0; ii<numPixels; ++ii) {
    float t = float(ii)/numPixels + fIdx;
    if(t>1.) { t-=1.f; }

    uint16_t pixelPos = uint8_t(t*LEDS_COUNT);
    pixelPos = min(LEDS_COUNT-1, pixelPos);

    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(t*0xff)<<8, 0xff, 0xff);
    color = Adafruit_NeoPixel::gamma32(color);

    LEDs::strip.setPixelColor(pixelPos, color);
  }

}

void HandleButtons() {
  // Check the buttons
  const auto& buttons = Buttons::check();

  if(buttons.checked) {
    CheckIncMode(buttons);

    if(buttons.state[Buttons::BtnA2] == Buttons::StatePressed) {
      // Serial.print("toggling edit state"); Serial.println(editState);
      numPixels += 1;
      if(numPixels > MAX_PIXELS) { numPixels = MIN_PIXELS; }
    }   
  }
}

}; // namespace OppositeMode