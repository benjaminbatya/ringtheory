#include <Arduino.h>

#include "Buttons.hpp"

namespace Buttons {

unsigned long lastTime;
Data data;

struct Button {
  int pin;
  const char* name;
};

// Button definitions
const Button Buttons[] = {
  {10, "A1"},
  { 9, "A2"},
  { 5, "B1"},
  { 8, "B2"},
  { 7, "B3"},
  { 6, "B4"}
};

const char* StateNames[] = {
  " Up, ", " Pressed, ", " Released, ", " Down, "
};

void printButtonStates();

void setup() {
  // make the pushbutton's pin an input:
  for(int ii=0; ii<Btn_COUNT; ++ii) {
    pinMode(Buttons[ii].pin, INPUT_PULLUP);
  }

  lastTime = 0;
}

Data const& check() {
  auto now = millis();

  data.checked = false;

  // early return if elapsed time is less then 100ms
  if(now - lastTime < 100) { return data; }
  lastTime = now;

  data.checked = true;

  // read the input pins
  for(int ii=0; ii<Btn_COUNT; ++ii) {
    // invert state
    int state = digitalRead(Buttons[ii].pin);
    // update the state
    /* Truth table for state to track Up(0),Press(1),Release(2),Down(3)
       H   L
     |-------|
    H|0x0|0x1|
     |-------|
    L|0x2|0x3|
     |-------|
     So the prev up/down state is data.state&0x1 and its one bool op to derive the next state
     */
    data.state[ii] = (((data.state[ii] & 0x1) << 1) | ((!state) & 0x1));
  }

  // print out the state of the buttons
  // printButtonStates();

  return data;
}

void printButtonStates() {
  bool pressed = false;
  for(int ii=0; ii<Btn_COUNT; ++ii) {
    const int state = data.state[ii];
    if(state!=StateUp) { 
      pressed = true; 
      Serial.print(Buttons[ii].name); 
      Serial.print(StateNames[state]); 
    }
  }

  if(pressed) { Serial.println(""); }
}

}; // namespace Input