#include <stdint.h>

#include "LEDs.hpp"
#include "Accel.hpp"
#include "main.hpp"

namespace DiffuseMode {

struct Pixel {
  uint8_t hue;
  uint8_t sat;
  uint8_t val;
};

const int SECTION_SIZE = 22;
const int NUM_SECTIONS = (LEDS_COUNT/SECTION_SIZE);
const int MID_IDX = SECTION_SIZE/2;

Pixel pixels[SECTION_SIZE] = {0x0};

int velocity[3];

void HandleAccel();
void Simulate();
void Render();

unsigned long lastTime = 0;
uint8_t newSat = 0xff;

void Setup() {
  memset(pixels, 0, sizeof(pixels));

  pixels[MID_IDX].hue = 0x80;
  pixels[MID_IDX].sat = 0x80;
  pixels[MID_IDX].val = 0x80;
  
  velocity[0] = 0;
  velocity[1] = 0; // (0xff/MID_IDX);
  velocity[2] = -(0xff/MID_IDX);

  lastTime = millis();
}

void Update() {
  HandleAccel();

  Simulate();

  Render();
}

void HandleAccel() {

  auto now = millis();

  if(now - lastTime < 200) return;

  lastTime = now;

  float accel2 = Accel::accelMag2();
  // Serial.printf("accel2 = %f\n", accel2);

  newSat = 255-uint8_t(((accel2-3600.f) / 20000.f)*255.f);
}

void Simulate() {

  for(int ii=0; ii<MID_IDX; ++ii) {
    pixels[ii] = pixels[ii+1];
    pixels[SECTION_SIZE-1-ii] = pixels[SECTION_SIZE-2-ii];
  }

  auto& pixel = pixels[MID_IDX];

  float fIdx = Accel::interpolatedTilt();
  pixel.hue = uint8_t(fIdx*0xff);

  pixel.sat = newSat;
  static const uint8_t SAT_INC = 0x50;
  if(newSat < 0xff-SAT_INC) { newSat += SAT_INC; }
  else { newSat = 0xff; }
  
  if((velocity[2]<0 && pixel.val<-velocity[2] ) ||
     (velocity[2]>0 && pixel.val>(0xff-velocity[2]))) { velocity[2] *= -1; }
  pixel.val += velocity[2];
}

void Render() {
  for(uint8_t ii=0; ii<SECTION_SIZE; ++ii) {
    
    const auto& pixel = pixels[ii];

    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(pixel.hue)<<8, pixel.sat, pixel.val);
    color = Adafruit_NeoPixel::gamma32(color);

    for(uint8_t jj=0; jj<NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(ii+jj*SECTION_SIZE, color);
    }
  }
}

}; // namespace DiffuseMode