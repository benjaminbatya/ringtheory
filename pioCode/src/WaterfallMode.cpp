#include "WaterfallMode.hpp"

#include "Accel.hpp"
#include "LEDs.hpp"

namespace WaterfallMode {

uint8_t direction = 0;

typedef struct Particle {
  struct Particle* next;

  uint8_t hue;
  // This has pos in the upper 7bits and velocity in the 0th bit
  int8_t pos;

} Particle;

static const uint8_t NUM_PARTICLES = 32;

Particle* dead = nullptr;
Particle* head = nullptr;
Particle* tail = nullptr;

// allocate static array of particles
Particle particles[NUM_PARTICLES] = {0};

// forward func decls
void Generate();
void Simulate();
void Render(); 

Particle* createHead();
Particle* createTail();
void configureParticle(Particle* part);

void killHead();
void killTail();

void Setup() {

  memset(particles, 0, sizeof(particles));

  for(int ii=0; ii<NUM_PARTICLES-1; ++ii) {
    (particles+ii)->next = particles+(ii+1);
  }

  dead = particles;
  head = nullptr;
  tail = nullptr;
}

void Update() {
  Simulate();

  Generate();

  Render();
}

void Generate() {

  uint16_t accel2 = Accel::accelMag2();
  // Serial.print("Accel: "); Serial.println(accel2);

  // if accel is high enough, generate a particle
  if(accel2 <= 5000) { return; }

  uint16_t xzAccel2 = Accel::xzMag2();
  auto yAccel = Accel::yValue(); 

  if(xzAccel2 < 300) { 
    // Serial.print("removing "); Serial.println(yAccel);

    if(abs(yAccel) > 10000) { 
      // erase the last created particle
      killHead();
    }

  } else if(xzAccel2 > 800) {
    // Serial.print("adding "); Serial.println(yAccel);

    // treat shaking as important only if there's a large enough Y component
    if(abs(yAccel) > 10000) { 
      // Build a particle
      // Make an particle
      auto part = createHead();

      configureParticle(part);
    }
  }
}

void Simulate() {
  auto ptr = head;
  while(ptr) {
    int8_t pos = ptr->pos;
    bool dir = pos & 0x1;
    int8_t vel = dir ? 1 : -1;
    pos >>=1;
    pos += vel; 
    ptr->pos = (pos<<1) | dir;
    ptr = ptr->next;
  }
}

void Render() {
  LEDs::strip.clear();

  for(auto part=head; part; part=part->next) {
    int8_t pos = part->pos>>1;
    auto idx = uint8_t(pos+LEDS_COUNT) % LEDS_COUNT;

    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(part->hue)<<8, 0xff, 0xff);
    color = Adafruit_NeoPixel::gamma32(color);
    LEDs::strip.setPixelColor(idx, color);
  }
}

void configureParticle(Particle* part) {
  // add particle to current tilt pos
  float fIdx = Accel::interpolatedTilt();
  auto newIdx = uint8_t(fIdx*LEDS_COUNT); 
  newIdx = min(LEDS_COUNT-1, newIdx);

  uint8_t hue = uint8_t(fIdx*0xff);
  part->hue = hue;
  part->pos = (newIdx<<1) | (direction&0x1);

  // shoot particles in opposite directions
  direction = (direction+1)%2;

  // Serial.print("configureParticle ");
  // Serial.println(part->hue);
}

Particle* createHead() {
  if(!dead) {
    // Serial.println("reallocating particle");
    // Convert an almost dead particle to a dead particle
    killTail();
  }

  Particle* part = dead;
  dead = dead->next;
  part->next = nullptr;

  if(!head) {
    head = part;
    tail = part;
  } else {
    // add particle to head
    part->next = head;
    head = part;
  }

  return part;
}

Particle* createTail() {
  if(!dead) {
    // Serial.println("reallocating particle");
    // Convert an almost dead particle to a dead particle
    killHead();
  }

  Particle* part = dead;
  dead = dead->next;
  part->next = nullptr;

  if(!head) {
    head = part;
    tail = part;
  } else {
    // add particle to tail
    tail->next = part;
    tail = part;
  }

  return part;
}

void killHead() {
  if(!head) { return; }

  auto part = head;

  if(head == tail) {
    head = nullptr;
    tail = nullptr; 
  } else {
    // remove particle from head list
    head = head->next;
  }

  // add it to the dead list
  part->next = dead;
  dead = part;

  // Serial.print("killHead ");
  // Serial.println(part->hue);
}

void killTail() {
  if(!tail) { return; }

  auto part = tail;

  if(head == tail) {
    head = nullptr;
    tail = nullptr;
  } else {
    // find the new tail
    for(tail = head; tail->next!=part; tail=tail->next) ; 
    tail->next = nullptr;
  }

  part->next = dead;
  dead = part;

  // Serial.print("killTail ");
  // Serial.println(part->hue);
}

}; // namespace WaterfallMode