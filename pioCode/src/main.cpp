// Must be included for compile to work!
#include <Arduino.h>

#include "main.hpp"
#include "LEDs.hpp"
#include "Buttons.hpp"
#include "Accel.hpp"

// Modes
#include "DarkMode.hpp"
#include "RotateMode.hpp"
#include "StackMode.hpp"
#include "OscillationMode.hpp"
// #include "WaveMode.hpp"
#include "OppositeMode.hpp"
#include "RainbowPixelMode.hpp"
#include "WaterfallMode.hpp"
#include "PaintMode.hpp"
#include "AutoMode.hpp"
#include "DiffuseMode.hpp"

void DefaultHandleButtons();

uint16_t DelayMS = 50;
void setupUpdate();
bool checkUpdate();
void setUpdate(uint16_t delay);

Mode modes[] = {
  // Always start with automatic mode
  { "AutoMode", AutoMode::Setup, AutoMode::Update, AutoMode::Cleanup },

  // Accelerameter modes
  { "DiffuseMode", DiffuseMode::Setup, DiffuseMode::Update },
  { "PaintMode", PaintMode::Setup, PaintMode::Update, nullptr, PaintMode::HandleButtons },
  { "WaterfallMode", WaterfallMode::Setup, WaterfallMode::Update },
  { "RainbowPixelMode", RainbowPixelMode::Setup, RainbowPixelMode::Update },
  { "OppositeMode", OppositeMode::Setup, OppositeMode::Update, nullptr, OppositeMode::HandleButtons },
  // { "WaveMode", WaveMode::Setup, WaveMode::Update},

  // Non-accelerameter modes
  { "OscillationMode", OscillationMode::Setup, OscillationMode::Update},
  { "StackMode", StackMode::Setup, StackMode::Update},
  { "RotateMode", RotateMode::Setup, RotateMode::Update},

  // Dark mode. Displays nothing...
  { "DarkMode", DarkMode::Setup, DarkMode::Update },

};
const uint8_t NUM_MODES = sizeof(modes)/sizeof(modes[0]);

int CurrentMode = 0;

// setup() function -- runs once at startup --------------------------------
void setMode(int newMode) {
  // tear down old mode
  if(modes[CurrentMode].cleanup) {
    modes[CurrentMode].cleanup();
  }


  CurrentMode = newMode;

  // Setup new mode
  Serial.printf("setMode: displaying '%s'\n", modes[CurrentMode].name);
  LEDs::strip.clear();
  modes[CurrentMode].setup();
  setupUpdate();
}

void setup() {
  // Only enable Serial for critical debugging. 
  // It will slowdown performance otherwise...
  Serial.begin(115200);
  Serial.println("RingTheory started");

  LEDs::init();

  Buttons::setup();

  Accel::Setup();

  // begin in first mode
  setMode(CurrentMode);
}

// loop() function -- runs repeatedly as long as board is on ---------------

void loop() {
  
  if(modes[CurrentMode].buttonHandler) {
    modes[CurrentMode].buttonHandler();
  } else {
    DefaultHandleButtons();
  }

  if(checkUpdate()) {
    // Always check accel for now...
    Accel::Update();

    modes[CurrentMode].update();
    LEDs::strip.show();
  }
}

void CheckIncMode(Buttons::Data const& data) {
  if(data.state[Buttons::BtnA1] == Buttons::StatePressed) {
    // go to the next mode
    setMode((CurrentMode+1) % NUM_MODES);
  }
}

void CheckSpeedControls(Buttons::Data const& data) {
  if(data.state[Buttons::BtnB3] == Buttons::StateDown) {
    setUpdate(DelayMS+5);
  } 
  if(data.state[Buttons::BtnB4] == Buttons::StateDown) {
    setUpdate(DelayMS-5);
  }
}

void DefaultHandleButtons() {
  auto data = Buttons::check();

  if(data.checked) {
    CheckIncMode(data);
    
    if(data.state[Buttons::BtnA2] == Buttons::StatePressed) {
      LEDs::incColorIdx();
      // reset the mode
      // TODO: make a smooth transition by color blending during the transition
      setMode(CurrentMode);
    }

    if(data.state[Buttons::BtnB1] == Buttons::StateDown) {
      // Serial.println("decBrightness");
      LEDs::decValue();
    } 
    if(data.state[Buttons::BtnB2] == Buttons::StateDown) {
      // Serial.println("incBrightness");
      LEDs::incValue();
    }

    CheckSpeedControls(data);
  }
}

// update functionality
unsigned long lastTime = 0; 
// static const int DELAY_MS = 50;

void setupUpdate() {
  lastTime = 0;
}

bool checkUpdate() {
  auto now = millis();
  // Wait a while between updates
  if(now - lastTime < DelayMS) { return false; }
  lastTime = now;

  return true;
}

void setUpdate(uint16_t delay) {
  // Serial.print("setting delay to "); Serial.println(delay);
  // clamp the delay values between 15 and 250ms
  delay = delay>10 ? delay : 10;
  delay = delay<500 ? delay : 500;
  DelayMS = delay;
}
