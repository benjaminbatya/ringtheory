#include <Arduino.h>

#include "OscillationMode.hpp"
#include "StackMode.hpp"
#include "RotateMode.hpp"

#include "main.hpp"
#include "LEDs.hpp"

namespace AutoMode {

struct ModeEx {
  Mode mode;
  int colorIdx;
};

// define
const ModeEx modes[] = {
  {{"O0", OscillationMode::Setup, OscillationMode::Update}, 0},
  {{"O2", OscillationMode::Setup, OscillationMode::Update}, 2},
  {{"O4", OscillationMode::Setup, OscillationMode::Update}, 4},

  {{"S0", StackMode::Setup, StackMode::Update}, 0},
  {{"S1", StackMode::Setup, StackMode::Update}, 1},
  {{"S2", StackMode::Setup, StackMode::Update}, 2},

  {{"R0", RotateMode::Setup, RotateMode::Update}, 0},
  {{"R2", RotateMode::Setup, RotateMode::Update}, 2},
  {{"R4", RotateMode::Setup, RotateMode::Update}, 4},

};

const int NUM_MODES = sizeof(modes) / sizeof(modes[0]);

int CurrentMode = 0;

uint8_t oldColorIdx = 0;

void setMode(int newMode);
void setupUpdate();
bool checkUpdate();

void Setup() {
  oldColorIdx = LEDs::getColorIdx();

  setupUpdate();

  setMode(CurrentMode);
}

void Cleanup() {
  // restore the color index
  LEDs::setColorIdx(oldColorIdx);
}

void Update() {

  if(checkUpdate()) { 
    setMode(CurrentMode+1);
  }

  modes[CurrentMode].mode.update();
}


void setMode(int newMode) {
  // tear down old mode

  // set the main
  CurrentMode = newMode % NUM_MODES;

  // Setup new mode
  Serial.printf("AutoMode::setMode: displaying '%s'\n", modes[CurrentMode].mode.name);
  LEDs::strip.clear();
  modes[CurrentMode].mode.setup();

  LEDs::setColorIdx(modes[CurrentMode].colorIdx);
}


// update functionality
unsigned long LastTime = 0; 
const int TIME_DELAY = 5*1000;

inline void setupUpdate() {
  LastTime = 0;
}

inline bool checkUpdate() {
  auto now = millis();
  // Wait a while between updates
  if(now - LastTime < TIME_DELAY) { return false; }
  LastTime = now;

  return true;
}

}; // namespace AutoMode