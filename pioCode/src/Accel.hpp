#include <stdint.h>

namespace Accel {

void Setup();
void Update();

int16_t xValue();
int16_t yValue();
int16_t zValue();
// squared magnitude of the XYZ accel vector
uint16_t accelMag2();
// squared magnitude of the XZ accel vector
uint16_t xzMag2();

// Returns a normalized value (0->1) representing the estimated tilt of the ring
// 0 and 1 represent +X facing up
float interpolatedTilt();

int16_t getYDiff();

}; // namespace Accel