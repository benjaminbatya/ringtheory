
#include <Adafruit_NeoPixel.h>

// How many NeoPixels are attached to the Arduino?
#define LEDS_COUNT 66
#define LEDS_NUM_SECTIONS 6
#define LEDS_IN_SECTION (LEDS_COUNT/LEDS_NUM_SECTIONS)

namespace LEDs {

extern ::uint8_t RED;   
extern ::uint8_t YELLOW;
extern ::uint8_t GREEN; 
extern ::uint8_t CYAN; 
extern ::uint8_t BLUE; 
extern ::uint8_t PURPLE;
extern ::uint8_t WHITE;

struct Palette2Tone {
  uint8_t primary;
  uint8_t secondary;
};

// Declare our NeoPixel strip object:
extern Adafruit_NeoPixel strip;

void init();

void incColorIdx();
void setColorIdx(uint8_t);
uint8_t getColorIdx();

void incValue();
void decValue();
uint8_t getValue(); 

}; // namespace LEDs