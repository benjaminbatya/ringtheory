#include "LEDs.hpp"
#include "Accel.hpp"

namespace RotateMode {

const LEDs::Palette2Tone PALETTE[] = {
  {LEDs::CYAN, LEDs::CYAN},
  {LEDs::PURPLE, LEDs::CYAN}, 
  {LEDs::YELLOW, LEDs::PURPLE}, 
  {LEDs::BLUE, LEDs::YELLOW},
  {LEDs::RED, LEDs::BLUE},
  {LEDs::GREEN, LEDs::GREEN},
  {LEDs::BLUE, LEDs::BLUE},
  {LEDs::PURPLE, LEDs::PURPLE},
  {LEDs::YELLOW, LEDs::YELLOW}, 
  {LEDs::RED, LEDs::RED},
  {LEDs::GREEN, LEDs::RED},
};

static const uint8_t NUM_COLORS = sizeof(PALETTE) / sizeof(PALETTE[0]);

uint8_t Count = 0;

inline void setupCounter() {
  Count = 0;
};
inline void updateCounter() {
  Count++;
  if(Count >= LEDS_IN_SECTION) { 
    Count = 0; 
  }
}

inline uint16_t getCounter() {
  return Count;
}

void Setup() {
  setupCounter();
}

void Update() {

  const auto palette = PALETTE[LEDs::getColorIdx() % NUM_COLORS];

  for(uint16_t ii=0; ii<LEDS_IN_SECTION; ++ii) { // For each pixel in strip...
    const uint16_t idx = (ii + getCounter()) % LEDS_IN_SECTION;

    uint8_t value = LEDs::getValue();
    uint8_t hue = 0;
    uint8_t saturation = 0xff;

    if((idx % 6) < 2) {
      // Draw primary
      hue = palette.primary;
      saturation = (palette.primary==palette.secondary ? 0 : 0xff);
    } else if(idx < 6) { 
      // Draw secondary
      hue = palette.secondary;

      // add in tilt for more interaction...
      float fIdx = Accel::interpolatedTilt();
      hue += uint8_t(fIdx*0xff);

    } else {
      // draw nothing
      value = 0;
    }

    // Serial.printf("hue=%u, value=%u\n", hue, value);

    uint32_t color = LEDs::strip.ColorHSV(uint16_t(hue)<<8, saturation, value);
    color = LEDs::strip.gamma32(color);
    for(int jj=0; jj<LEDS_NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(ii+jj*LEDS_IN_SECTION, color);
    }
  };

  updateCounter();
};

}; // namespace RotateMode