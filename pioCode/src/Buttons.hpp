namespace Buttons {

enum Btn{
  BtnA1 = 0, 
  BtnA2,

  BtnB1, 
  BtnB2, 
  BtnB3, 
  BtnB4,

  Btn_COUNT
};

const int StateUp = 0x0;
const int StatePressed = 0x1; // state & 0x1 == prev down
const int StateReleased = 0x2;
const int StateDown = 0x3;

struct Data {
  // indicates the state of the various buttons
  int state[Btn_COUNT] = {StateUp};
  // Indicates if the buttons were checked or not
  bool checked;
};

void setup();
// Checks the buttons and returns the current state
Data const& check();

}; // namespace Buttons

