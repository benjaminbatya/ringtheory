#include "StackMode.hpp"
#include "LEDs.hpp"
#include "Accel.hpp"

namespace StackMode {

const LEDs::Palette2Tone PALETTE[] = {
  {LEDs::PURPLE, LEDs::YELLOW}, 
  {LEDs::RED, LEDs::RED},
  {LEDs::BLUE, LEDs::YELLOW},
  {LEDs::PURPLE, LEDs::PURPLE},
  {LEDs::RED, LEDs::BLUE},
  {LEDs::GREEN, LEDs::GREEN},
  {LEDs::YELLOW, LEDs::PURPLE}, 
  {LEDs::BLUE, LEDs::BLUE},
  {LEDs::YELLOW, LEDs::YELLOW},
  {LEDs::GREEN, LEDs::RED},
};
static const uint8_t NUM_COLORS = sizeof(PALETTE) / sizeof(PALETTE[0]);

int8_t Pos = 0;
bool Filled[LEDS_IN_SECTION];

void Reset();
void Simulate();

void Setup() {
  Reset();
};

void Update() {
  Simulate();

  const auto palette = PALETTE[LEDs::getColorIdx() % NUM_COLORS];

  for(uint16_t ii=0; ii<LEDS_IN_SECTION; ++ii) { // For each pixel in strip...    
    uint8_t hue = palette.secondary;
    uint8_t value = LEDs::getValue();
    if(!Filled[ii % LEDS_IN_SECTION]) {
      hue = palette.primary;
      if(palette.primary==palette.secondary) { value = 0x0; }
    } else {
      // add in tilt for more interaction...
      float fIdx = Accel::interpolatedTilt();
      hue += uint8_t(fIdx*0xff);
    }

    auto color = Adafruit_NeoPixel::ColorHSV(uint16_t(hue)<<8, 0xff, value);
    color = Adafruit_NeoPixel::gamma32(color);
    for(int jj=0; jj<LEDS_NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(ii+jj*LEDS_IN_SECTION, color);
    }
  }
};

void Simulate() {
  if(Pos >= LEDS_IN_SECTION || Filled[Pos]) {
    if(Pos == 1) {
      Reset();
    }
    Pos = 0;
  }
  
  Filled[Pos]= true;
  if(Pos > 0) { Filled[Pos-1] = false; }
  Pos++;
}

void Reset() {
  for(int ii=0; ii<LEDS_IN_SECTION; ++ii) {
    Filled[ii] = false;
  }
}

}; // namespace StackMode
