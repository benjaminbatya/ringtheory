
#include "LEDs.hpp"
#include "Accel.hpp"

namespace OscillationMode {

const uint8_t HUES[] = {
  LEDs::WHITE,
  LEDs::CYAN,
  LEDs::PURPLE,
  LEDs::YELLOW,
  LEDs::BLUE, 
  LEDs::RED, 
  LEDs::GREEN, 
};
static const uint8_t NUM_COLORS = sizeof(HUES) / sizeof(HUES[0]);

uint8_t Count = 0;

inline void setupCounter() {
  Count = 0;
};
inline void updateCounter() {
  Count++;
  if(Count >= LEDS_IN_SECTION) { 
    Count = 0; 
  }
}

inline uint16_t getCounter() {
  return Count;
}

// 4 leds increasing value, 4 decreasing, 3 blank
void Setup() {
  setupCounter();
}

void Update() {

  uint8_t hue = HUES[LEDs::getColorIdx() % NUM_COLORS];
  // display white
  uint8_t saturation = 0xff;
  if(hue == LEDs::WHITE) { saturation = 0x0; }
  
  // add in tilt for more interaction...
  if( hue != LEDs::WHITE) {
    float fIdx = Accel::interpolatedTilt();
    hue += uint8_t(fIdx*0xff);
  }

  const uint16_t finalHue = hue << 8;



  // Serial.printf("hue=%d, saturation=%d\n", hue, saturation);

  for(uint16_t ii=0; ii<LEDS_IN_SECTION; ++ii) { // For each pixel in section...
    const uint16_t idx = (ii + getCounter()) % LEDS_IN_SECTION;

    uint16_t value = 0x0;

    if(idx < 4) { value = 64*(idx+1) - 1; }
    else if(idx < 8) { value = 64*(8-idx) - 1; }

  // scale by brightness
    value *= LEDs::getValue();
    value >>= 8;

    uint32_t color = LEDs::strip.ColorHSV(finalHue, saturation, uint8_t(value));
    color = LEDs::strip.gamma32(color);
    for(int jj=0; jj<LEDS_NUM_SECTIONS; ++jj) {
      LEDs::strip.setPixelColor(ii+jj*LEDS_IN_SECTION, color);
    }
  }

  updateCounter();
};

}; // namespace OscillationMode