struct Mode {
  const char* name;

  using Func = void(*)();
  Func setup;
  Func update;

  Func cleanup;

  Func buttonHandler;
};

namespace Buttons {
struct Data;
};

void CheckIncMode(Buttons::Data const& data);
void CheckSpeedControls(Buttons::Data const& data);