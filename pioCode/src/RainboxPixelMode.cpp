#include "Accel.hpp"
#include "LEDs.hpp"

namespace RainbowPixelMode {

uint8_t currentHue = 0;
uint8_t pixelIdx = 0xff;

typedef struct  {
  uint8_t hue;
  uint8_t value;
} Pixel;

Pixel pixels[LEDS_COUNT];

void Setup() {

  currentHue = 0;
  pixelIdx = 0xff;

  for(Pixel* ptr=pixels; ptr<pixels+LEDS_COUNT; ++ptr) {
    ptr->value = 0;
  }
  LEDs::strip.clear();
}

void Update() {

  // Update the active pixel
  float fIdx = Accel::interpolatedTilt();

  auto newIdx = uint8_t(fIdx*LEDS_COUNT); 
  newIdx = min(LEDS_COUNT-1, newIdx);

  if(pixelIdx!=0xff && pixelIdx!=newIdx) {
    // fill in pixels between pixelIdx and newIdx with color
    int inc = newIdx>pixelIdx ? 1 : -1;
    int dist = (newIdx-pixelIdx)*inc;

    if(dist >= LEDS_COUNT/2) {
      inc *= -1;
      dist = LEDS_COUNT-dist;
    }
    for(int ii=1; ii<dist; ++ii) {
      uint8_t idx = (pixelIdx+(ii)*inc) % LEDS_COUNT;
      pixels[idx].value = 0xff-0xf*(dist-ii);
      pixels[idx].hue = currentHue-0xf*(dist-ii);
    }
  }
  pixels[newIdx].hue = currentHue;
  pixels[newIdx].value = 0xff;
  pixelIdx = newIdx;

  // This will naturally wrap around
  currentHue++;

  // Age the pixels and display them
  for(int ii=0; ii<LEDS_COUNT; ++ii) {
    auto ptr = pixels+ii;
    if(ptr->value == 0) { continue; }

    ptr->value -= 0xf;
    ptr->hue -= 0xf;

    uint32_t color = Adafruit_NeoPixel::ColorHSV(uint16_t(ptr->hue)<<8, 0xff, ptr->value);
    color = Adafruit_NeoPixel::gamma32(color);
    LEDs::strip.setPixelColor(ii, color);
  }
}

}; // namespace RainbowPixelMode
