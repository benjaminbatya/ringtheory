#include <Adafruit_Sensor.h>
#include <Adafruit_LIS3DH.h>

#include "Accel.hpp"

namespace Accel {

#define ACCEL_RANGE      LIS3DH_RANGE_2_G  // Range of acceleration values for the LIS3DH.  Can
                                           // change to 2G, 4G, 8G, or 16G values.

#define SAMPLE_SIZE      4                 // Number of samples to use for a moving average of
                                           // on each accelerometer axis.  This smoothes out the
                                           // readings so they're less noisey.

// Keep a moving average of the last few accelerometer samples to smooth out the readings.
// This helps reduce random noise and spikes/jumps in the data.
int16_t _xSamples[SAMPLE_SIZE] = { 0 };
int16_t _ySamples[SAMPLE_SIZE] = { 0 };
int16_t _zSamples[SAMPLE_SIZE] = { 0 };
int16_t _avgValues[3] = { 0 };

uint16_t _accelMag2 = 0;
uint16_t _xzAccel2 = 0;

int _samplePos = 0;  // use round robin to gather samples

// Create LIS3DH accelerometer library object, this will by default use an I2C connection.
Adafruit_LIS3DH accel = Adafruit_LIS3DH();

void Setup() {
  // Initialize LIS3DH accelerometer library.
  if (!accel.begin()) {
    Serial.println("Couldn't find LIS3DH, is it connected?");
    while(1);
  }
  accel.setRange(ACCEL_RANGE);
}

int16_t avgSamples(const int16_t samples[SAMPLE_SIZE]);

void Update() {
  // Take an accelerometer reading and add to lists of samples.
  accel.read();
  _xSamples[_samplePos] = accel.x;
  _ySamples[_samplePos] = accel.y;
  _zSamples[_samplePos] = accel.z;

  _samplePos = (_samplePos+1)%SAMPLE_SIZE;

  _avgValues[0] = avgSamples(_xSamples);
  _avgValues[1] = avgSamples(_ySamples);
  _avgValues[2] = avgSamples(_zSamples);  

  // Divide by 256 to avoid overflow
  int16_t x = accel.x>>8;
  int16_t y = accel.y>>8;
  int16_t z = accel.z>>8;
  // NOTE: This is not averaged over several values but is the last value only for now...
  _accelMag2 = uint16_t(x*x) + uint16_t(y*y) + uint16_t(z*z);
  _xzAccel2 = uint16_t(x*x) + uint16_t(z*z);

  // if(_accelMag2 > 5000) {
  //   float accel2 
  //     = accel.x_g*accel.x_g 
  //     + accel.y_g*accel.y_g 
  //     + accel.z_g*accel.z_g;

  //   Serial.print(" \tAccel: "); Serial.print(sqrt(accel2));
  //   Serial.print(" \tSq: "); Serial.print(accel2);
  //   Serial.print(" m/s^2 ");

  //   Serial.print(" \tX: "); Serial.print(_avgValues[0]);
  //   Serial.print("  \tY: "); Serial.print(_avgValues[1]);
  //   Serial.print(" \tZ: "); Serial.print(_avgValues[2]);
  //   Serial.print(" \tAccel2: "); Serial.print(_accelMag2);

  //   Serial.print(" \txzAccel2: "); Serial.print(_xzAccel2);

  //   Serial.println(" int ");
  // }

}

int16_t avgSamples(const int16_t samples[SAMPLE_SIZE]) {
  int32_t ret = 0;
  for(int ii=0; ii<SAMPLE_SIZE; ++ii) {
    ret += samples[ii];
  }
  return ret/SAMPLE_SIZE;
}

int16_t xValue() { return _avgValues[0]; }
int16_t yValue() { return _avgValues[1]; }
int16_t zValue() { return _avgValues[2]; }
uint16_t accelMag2() { return _accelMag2;}
uint16_t xzMag2() { return _xzAccel2; }

float interpolatedTilt() {
  // ignore the Y value. only use X&Z
  const auto x = float(Accel::xValue());
  const auto z = float(Accel::zValue());

  const float angle = atan2(x, z);
  float fIdx = (angle+M_PI) / (M_PI*2);
  return fIdx;
}

int16_t getXDiff() {
  auto prevPos = (_samplePos+SAMPLE_SIZE-1)%SAMPLE_SIZE;

  auto value = _xSamples[_samplePos];
  auto prev = _xSamples[prevPos];

  auto diff = value - prev;
  return diff;
}

int16_t getYDiff() {
  auto prevPos = (_samplePos+SAMPLE_SIZE-1)%SAMPLE_SIZE;

  auto value = _ySamples[_samplePos];
  auto prev = _ySamples[prevPos];

  auto diff = value - prev;
  return diff;
}

int16_t getZDiff() {
  auto prevPos = (_samplePos+SAMPLE_SIZE-1)%SAMPLE_SIZE;

  auto value = _zSamples[_samplePos];
  auto prev = _zSamples[prevPos];

  auto diff = value - prev;
  return diff;
}

}; // namespace Accel