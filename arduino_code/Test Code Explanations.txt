***68 LEDs should be the number of LEDs considered. The board has a 12MHz crystal. 

acceldemoRingTheory        -------   Test Code that, when board is connected via serial, should display accelerometer data on the arduino serial monitor.

DigitalReadSerial_RingTheory -----   Test Code that, when board is connected via serial, displays whether any of the six buttons are triggered or not on the serial monitor. Pin definitions for the buttons are included here. 

strandtest_RingTheory ---- Test code that drives a rainbow pattern on the Ring Theory hoop LEDs. Includes definition for LED pin and number of LEDs. 68 LEDs are to be used for the project (even though the prototype you have may not have 68 LEDs)

strandtest_RingThery_Accel -- Test code that causes LEDs to be driven to reflect the accelerometer value. Flips from blue to red across one accelerometer axis. 

