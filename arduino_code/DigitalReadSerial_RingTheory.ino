/*
  DigitalReadSerial

  Reads a digital input on pin 2, prints the result to the Serial Monitor

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/DigitalReadSerial
*/

// digital pin 2 has a pushbutton attached to it. Give it a name:
int ButtonA1 = 10; // A1
int ButtonA2 = 9; // A2
int ButtonB1 = 5; //B1
int ButtonB2 = 8; //B2
int ButtonB3 = 7; //B3
int ButtonB4 = 6; //B4


// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(ButtonA1, INPUT_PULLUP);
  pinMode(ButtonA2, INPUT_PULLUP);
  pinMode(ButtonB1, INPUT_PULLUP);
  pinMode(ButtonB2, INPUT_PULLUP);
  pinMode(ButtonB3, INPUT_PULLUP);
  pinMode(ButtonB4, INPUT_PULLUP);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int buttonStateA1 = digitalRead(ButtonA1);
  int buttonStateA2 = digitalRead(ButtonA2);
  int buttonStateB1 = digitalRead(ButtonB1);
  int buttonStateB2 = digitalRead(ButtonB2);
  int buttonStateB3 = digitalRead(ButtonB3);
  int buttonStateB4 = digitalRead(ButtonB4);
  // print out the state of the button:
  Serial.println(".............................................");
  Serial.println("A1  "); Serial.println(buttonStateA1);
  Serial.println("A2  "); Serial.println(buttonStateA2);
  Serial.println("B1  "); Serial.println(buttonStateB1);
  Serial.println("B2  "); Serial.println(buttonStateB2);
  Serial.println("B3  "); Serial.println(buttonStateB3);
  Serial.println("B4  "); Serial.println(buttonStateB4);
  delay(1000);        // delay in between reads for stability
}
